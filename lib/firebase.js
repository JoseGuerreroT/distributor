import * as firebase from 'firebase'

class Firebase{
    static init(){
        firebase.initializeApp({
            apiKey: "AIzaSyBKRSnnqyPszq_hXVfDfX3Qd84Ib4nyqpY",
            authDomain: "distributor-e2f93.firebaseapp.com",
            databaseURL: "https://distributor-e2f93.firebaseio.com",
            projectId: "distributor-e2f93",
            storageBucket: "distributor-e2f93.appspot.com",
        })
    }
}

export default Firebase;