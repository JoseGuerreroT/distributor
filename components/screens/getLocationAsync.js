import {Location, Permissions } from 'expo';

export default _getLocationAsync = async () => {
    let errorMessage;
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
        errorMessage = 'Permission to access location was denied';
    }
    var options = {
        enableHighAccuracy: true,
        maximumAge: 0
      };
    let location = await Location.getCurrentPositionAsync(options);
    const {latitude,longitude} = location.coords;
    let respuesta = {
        latitude,
        longitude,
        errorMessage,
    }
    return respuesta; 
};