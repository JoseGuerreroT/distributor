import React, { Component } from 'react';
import { Container, Button, Content, Form, Item, Input, Text, Picker } from 'native-base';
import save from './../../services/save'

class FormSimple extends Component {

    constructor({longitude, latitude}) {
        super();
        this.state = {
            cedula: '',
            nombre: '',
            colorHouse: '#ecf0f1',
            longitude,
            latitude,
         };
      }

      componentWillReceiveProps({longitude, latitude}) {
        this.setState({longitude, latitude});
      }

      onValueChange2(colorHouse) {
        this.setState({ colorHouse });
      }
      SaveRepresentantive = () =>{
        alert(save(this.state));
      }

    render() {
        console.log(this.state);
        return (
          <Container>
            <Content>
              <Form>
                <Item>
                  <Input numeric placeholder="Cedula" onChangeText={(cedula) => this.setState({cedula})} />
                </Item>
                <Item last>
                  <Input placeholder="Nombre completo" onChangeText={(nombre) => this.setState({nombre})} />
                </Item>
                
                <Item picker>
                  <Picker
                    mode="dropdown"
                    style={{ width: undefined }}
                    placeholder="Select your SIM"
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.colorHouse}
                    onValueChange={this.onValueChange2.bind(this)}
                  >
                    <Picker.Item label="Blanco" value="#ecf0f1" />
                    <Picker.Item label="Amarillo" value="#f1c40f" />
                    <Picker.Item label="Azul" value="#3498db" />
                    <Picker.Item label="Rojo" value="#c0392b" />
                    <Picker.Item label="Verde" value="#27ae60" />
                    <Picker.Item label="Morado" value="#673AB7" />
                    <Picker.Item label="Naranja" value="#FF5722" />
                    <Picker.Item label="Gris" value="#9E9E9E" />
                  </Picker>
              </Item>

                <Button rounded block success onPress={this.SaveRepresentantive}><Text> Guardar </Text></Button>
              </Form>

            </Content>
          </Container>
        );
      }
}



export default FormSimple;