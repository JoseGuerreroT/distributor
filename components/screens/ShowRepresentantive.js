import React, { Component } from 'react';
import { Container, Header, Button, Content, Item, Input, Text, Icon } from 'native-base';
import ItemRepresentative from './ItemRepresentative';
import firebase from './../../services/connect'
import handleSearch from './../../services/search'
import getLocationAsync from './getLocationAsync'



// Fix mensaje amarillo
import _ from 'lodash'
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};
// Fin fix mensaje amarillo

class ShowRepresentantive extends Component {

    constructor(){
        super();
        console.ignoredYellowBox = [
            'Setting a timer'
            ];
        this.state = {
          representatives: '',
          searchTerm: '',
          origin: '',
        };
    }


    strToComponents = (represent,origin) => (
    
      represent ? Object.keys( represent ).map((client) => (
            <ItemRepresentative
                    key={represent[client].cedula}
                    cedula={represent[client].cedula}
                    nombre={represent[client].nombre}
                    colorHouse={represent[client].colorHouse}
                    longitude={represent[client].longitude}
                    latitude={represent[client].latitude}
                    origin={origin}
            />
          )
          ): <Text>No hay datos</Text>
    );

    isSearch = (represent, origin) =>{
      if(this.state.searchTerm !== ''){
        represent = handleSearch(this.state.searchTerm, represent);
      }
      return this.strToComponents(represent,origin);
    }


    componentWillMount(){
        const that = this;
         firebase.database().ref('/represent').on('value', function (snapshot) {
          const representatives = snapshot.val();
          console.log("REPRESENTATIVES:")
          console.log(representatives)
          that.setState({representatives})
        });
       
          getLocationAsync().then((respuesta) => {
              const {latitude, longitude} = respuesta;
              const origin = {latitude,longitude}
              console.log("GETLOCATION")
              console.log(origin)
              this.setState({origin})
          })
        
        
    }

    render() {
      const origin = this.state.origin;
        return (
            <Container>
              <Header searchBar rounded>
              <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search" onChangeText={(term) => this.setState({searchTerm:term})} />
                <Icon name="ios-people" />
              </Item>
              <Button transparent>
                <Text>Search</Text>
              </Button>
            </Header>
           {this.state.representatives !== '' ? <Content>
                {this.isSearch(this.state.representatives, origin)}
            </Content> 
            :<Text>Cargando representantes...</Text>}
          </Container>
        );
    }

}

export default ShowRepresentantive;
