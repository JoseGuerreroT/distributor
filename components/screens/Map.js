import React, { Component } from 'react';
import { MapView, Marker,Font } from 'expo';
import { View, Text } from 'react-native';

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
        latitudeD: props.latitudeDestin,
        longitudeD: props.longitudeDestin,
        latitudeO: props.latitudeOrigin, //0.0092
        longitudeo: props.longitudeOrigin, // 0.0021
        colorHouse: props.colorHouse,
    };
  }
  // TODO: style={{ flex: 1.9 }} en dodne se llama el componente 
  render() {
    const {latitude,longitude,latitudeDelta,latitudeDelta} = this.state;
    return (
        <MapView 
                    region={{
                        latitude,
                        longitude,
                        latitudeDelta,
                        longitudeDelta,
                    }}
                    onPress={({nativeEvent}) => {
                                            const {longitude, latitude} = nativeEvent.coordinate;
                                            this.setState({longitude, latitude})
                                        }
                            }
                >
                <MapView.Marker
                    coordinate={{latitude,longitude}}
                    title={"Seleccionada"}
                    description={longitude + ", " + latitude}
                />
        </MapView>
    );
  }
}
