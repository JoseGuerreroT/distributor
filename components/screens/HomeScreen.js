import React, { Component } from 'react';
import {View } from 'react-native';
import { MapView, Marker,Font } from 'expo';
import {Location, Permissions } from 'expo';
import Setting from './ShowRepresentantive'
import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import { Container, Header, Content, Form, Item, Input, Icon, Button, Text, Body, Title, Right, Left } from 'native-base';
import FormSimple from './FormSimple'
import getLocationAsync from './getLocationAsync'


class HomeScreen extends Component {
    state = {
        longitude: null,
        latitude: null,
        errorMessage: null,
      };

      static navigationOptions = {
        title: "Hola",
        headerRight: (
          <Button
            onPress={() => alert('This is a button!')}
            title="Info"
            color="#fff"
          />
        ),
      };

    componentWillMount() {
        getLocationAsync().then((respuesta) => {
            const {latitude, longitude} = respuesta;
            this.setState({latitude, longitude})
        })
        Font.loadAsync({
            'Roboto': require('native-base/Fonts/Roboto.ttf'),
            'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        });
    }


    render() {
        if(this.state.latitude){
            const {latitude,longitude} = this.state;
            console.log("LONGGGGG: "+longitude)
            return (
                <View style={{flex:1}}>
                <Header>
                    <Left/>
                    <Body>
                        <Title>Guardar ubicación</Title>
                    </Body>
                    <Right>
                        <Icon name='home' style={{color:'white'}} />
                    </Right>
                </Header>
                <MapView style={{ flex: 1.9 }}
                    region={{
                        latitude,
                        longitude,
                        latitudeDelta: 0.0092,
                        longitudeDelta: 0.0021,
                    }}
                    onPress={({nativeEvent}) => {
                                            const {longitude, latitude} = nativeEvent.coordinate;
                                            this.setState({longitude, latitude})
                                        }
                            }
                >  
                <MapView.Marker
                            coordinate={{latitude,longitude}}
                            title={"Seleccionada"}
                            description={longitude + ", " + latitude}
                            />
                </MapView>
                <FormSimple style={{flex: 0.3}} longitude={longitude} latitude={latitude}/>
               </View>
            );
        }else
            return ( <Text style={{flex:1}}>Cargando....</Text>)
    }
}




// ---------------------
const MyDrawerNavigator = createDrawerNavigator({
    Buscar: {
      screen: Setting,
    }
  });
  
const MyApp = createAppContainer(MyDrawerNavigator);
export default HomeScreen;