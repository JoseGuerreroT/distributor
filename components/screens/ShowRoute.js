import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert} from 'react-native';
import MapViewDirections from './MapDirection'
import { MapView, Marker,Font } from 'expo';
import {Header} from 'native-base'

const GOOGLE_MAPS_APIKEY = 'AIzaSyCYvMpmVhFc0ydILEuXGJNYNGFnBoKPCL8';

class ShowRoute extends Component {
  
  constructor({origin,representative}){
    super();
    this.state = {
      origin,
      destination : {latitude: representative.latitude, longitude: representative.longitude},
      representative,
      modalVisible: false,
    };
  }
  handleFunction(){
    this.setState({modalVisible: true})
  }
 
  render() {
    console.log("MODAL: ")
    console.log(this.state.representative)
    console.log("Este es el origin:: ")
    console.log(this.state.origin)
    if(!this.state.modalVisible){
      return null;
    }else{
      return (
        
        <View style={{marginTop: 22}}>
        {this.state.origin ?
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({modalVisible: false})
            }}>
            <Header></Header>
            
            <MapView style={{flex:1}}initialRegion={{
                					latitude: this.state.origin.latitude,
                          longitude:this.state.origin.longitude,
                          latitudeDelta: 0.0092,
                          longitudeDelta: 0.0021,
            }}>
              <MapViewDirections
                origin={this.state.origin}
                destination={this.state.destination}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={4}
                strokeColor="#c0392b"
              />
            </MapView>
           
          </Modal>:
          <Text>Cargando...</Text>
          }
        </View>
      );
    }
  }
}

export default ShowRoute