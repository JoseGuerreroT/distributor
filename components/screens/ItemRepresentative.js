import React, { Component } from 'react';
import { Button, ListItem, Text, Icon, Left, Body, Right, View, Header } from 'native-base';
import ShowRoute from './ShowRoute'
import getLocationAsync from './getLocationAsync'


// TODO: Añadir modal https://facebook.github.io/react-native/docs/modal

class ItemRepresentative extends Component {

  constructor({cedula, nombre, colorHouse, longitude, latitude, origin }){
        super();
        this.state = {
            cedula,
            nombre,
            colorHouse,
            longitude,
            latitude,
            origin,
        };
    }
    handleChildState = () => {
      this.setState({modalVisible:false});
    }
    
    render() {
          const {cedula, nombre, colorHouse} = this.state;
          return (
              <View>
                <ListItem avatar>
                  <Left>
                  <Button style={{ backgroundColor: "#222f3e" }}  onPress={() => (this.showRoute.handleFunction())}>
                    <Icon style={{fontSize: 29, color: colorHouse}} active name="home" />
                    </Button>
                  </Left>
                  <Body>
                    <Text>{nombre}</Text>
                    <Text note>More description and references</Text>
                  </Body>
                  <Right>
                    <Text note>CC.{cedula}</Text>
                  </Right>
                  <ShowRoute origin={this.state.origin} representative={this.state} ref={ref => (this.showRoute = ref)}></ShowRoute>
                </ListItem>
              </View>
          );
    }
}

export default ItemRepresentative;