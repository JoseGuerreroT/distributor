import React, {Component} from 'react'
import { Ionicons } from '@expo/vector-icons'; 
import IconWithBadge from './../IconWithBadges'

const getTabBarIcon = (navigation, focused, tintColor) => {
    const { routeName } = navigation.state;
    let IconComponent = Ionicons;
    let iconName;
    if (routeName === 'Home') {
      iconName = `ios-information-circle`;
      // We want to add badges to home tab icon
      IconComponent = HomeIconWithBadge;
    } else if (routeName === 'Settings') {
      iconName = `ios-add-circle${focused ? '' : '-outline'}`;
    }
  
    // You can return any component that you like here!
    return <IconComponent name={iconName} size={25} color={tintColor} />;
  };

  const HomeIconWithBadge = props => {
    // You should pass down the badgeCount in some other ways like context, redux, mobx or event emitters.
    return <IconWithBadge {...props} badgeCount={3} />;
  };

  export default getTabBarIcon;