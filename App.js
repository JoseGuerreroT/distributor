import React, {Component} from 'react';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import getTabBarIcon from './components/constants/getTabBarIcon'
import HomeScreen from './components/screens/HomeScreen'
import ShowRepresentantive from './components/screens/ShowRepresentantive'


export default class App extends Component {
  render() {
    return (
      <TabMenu/>
    );
  }

}


const TabMenu = createAppContainer(
  createBottomTabNavigator(
    {
      Home: HomeScreen,
      Mostrar: ShowRepresentantive,
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) =>
          getTabBarIcon(navigation, focused, tintColor),
      }),
      tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      },
    }
  )
);