import _ from 'lodash'

const contains = ({cedula, nombre}, query) => {
    const queryString = query.toString();
    if(cedula.includes(query) || (nombre.toLowerCase()).includes(queryString.toLowerCase())){
      return true;
    }
    return false;
  }

  const handleSearch = (text, representatives) => (
    _.filter(representatives, user => (
        contains(user, text)
      )
    )
  )

  export default handleSearch;